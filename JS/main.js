$(document).ready(function(){
	$('#butoane ul').show();
	$('#imagini').css('overflow','hidden');
	$('#butoane ul>li:first-child').click(function(){
		$('#butoane ul img').attr("src","img/b_off.png");
		$('#butoane ul>li:first-child>img').attr("src","img/b_on.png");
		$('#imagini ul').css('margin-left','0px');
	});
	$('#butoane ul>li:nth-child(2)').click(function(){
		$('#butoane ul img').attr("src","img/b_off.png");
		$('#butoane ul>li:nth-child(2)>img').attr("src","img/b_on.png");
		$('#imagini ul').css('margin-left','-671px');
	});
	$('#butoane ul>li:nth-child(3)').click(function(){
		$('#butoane ul img').attr("src","img/b_off.png");
		$('#butoane ul>li:nth-child(3)>img').attr("src","img/b_on.png");
		$('#imagini ul').css('margin-left','-1342px');
	});
	$('#butoane ul>li:nth-child(4)').click(function(){
		$('#butoane ul img').attr("src","img/b_off.png");
		$('#butoane ul>li:nth-child(4)>img').attr("src","img/b_on.png");
		$('#imagini ul').css('margin-left','-2013px');
	});
	$('#butoane ul>li:last-child').click(function(){
		$('#butoane ul img').attr("src","img/b_off.png");
		$('#butoane ul>li:last-child>img').attr("src","img/b_on.png");
		$('#imagini ul').css('margin-left','-2684px');
	});
	/*dreapta*/
	$('#content li:first-child').click(function(){
		$(this).siblings().css({'background-color':'#ffcccc','border-bottom':'1px solid #dedede'});
		$(this).css({'background':'white','border-bottom':'0'});
		$('#content p').text('Popular');
	});
	$('#content li:nth-child(2)').click(function(){
		$(this).siblings().css({'background-color':'#ffcccc','border-bottom':'1px solid #dedede'});
		$(this).css({'background':'white','border-bottom':'0'});
		$('#content p').text('Recent');
	});
	$('#content li:nth-child(3)').click(function(){
		$(this).siblings().css({'background-color':'#ffcccc','border-bottom':'1px solid #dedede'});
		$(this).css({'background':'white','border-bottom':'0'});
		$('#content p').text('Comments');
	});
	$('#content li:last-child').click(function(){
		$(this).siblings().css({'background-color':'#ffcccc','border-bottom':'1px solid #dedede'});
		$(this).css({'background':'white','border-bottom':'0'});
		$('#content p').text('Tags');
	});
	$('input[type=text]').click(function(){
		$(this).removeAttr('value'); 
	});
	$('input[type=text]').focusout(function(){
		$(this).removeAttr('value'); 
		$(this).attr('value','Search here');
	});
	var topspace = 0;
	for(var i=0;i<501;i++){
	$('div#lines').append('<hr style="top:'+topspace+'px"/>');
	topspace+=5;
	};
	for(var i=0;i<12;i++){
	$('div#center').append('<span id="wide"><span></span><span></span></span>');
	};
	$('div#lines').click(function(){$('div#lines').hide();});
});